

import pandas as pd
import plotly.offline as pyo
import plotly.graph_objects as go

# create a DataFrame from the .csv file:
df = pd.read_csv('C:/Temp/DATA8inch.csv')

data=[go.Heatmap(x=df['X'], y=df['Y'], z=df['parm1'].values.tolist(),
                showscale=False,
                coloraxis=None
                 )]
layout=go.Layout(xaxis =  dict(showgrid=False,
                               ticks='',
                               showticklabels=False),
                 yaxis =  dict(showgrid=False,
                               ticks='',
                               showticklabels=False
                               ),
                 paper_bgcolor='rgba(0,0,0,0)',
                 plot_bgcolor='rgba(0,0,0,0)',
                 autosize=False,
                 width=1000,
                 height=1000
                 )
fig= go.Figure(data=data,layout=layout)
pyo.plot(fig)
##

import plotly
import plotly.io as pio
plotly.io.orca.config.executable = 'C:/Users/bdesa1000237653/AppData/Local/Programs/orca/orca.exe'
pio.renderers.default='png'
fig.show()
fig.write_image("fig1.png")

